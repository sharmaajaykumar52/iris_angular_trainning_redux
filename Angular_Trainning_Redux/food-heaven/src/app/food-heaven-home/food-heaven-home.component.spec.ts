import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodHeavenHomeComponent } from './food-heaven-home.component';

describe('FoodHeavenHomeComponent', () => {
  let component: FoodHeavenHomeComponent;
  let fixture: ComponentFixture<FoodHeavenHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodHeavenHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodHeavenHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

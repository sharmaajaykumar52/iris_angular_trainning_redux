import { ActionReducerMap } from '@ngrx/store';
import { FoodHeavenReducer } from './FoodHeavenReducer';
import { FoodHeavenState } from '../model/FoodHeavenState';

export const rootReducer = {};

export interface AppState {
    foodHeavenState: FoodHeavenState;
};

export const reducers: ActionReducerMap<AppState, any> = {
    foodHeavenState: FoodHeavenReducer
};
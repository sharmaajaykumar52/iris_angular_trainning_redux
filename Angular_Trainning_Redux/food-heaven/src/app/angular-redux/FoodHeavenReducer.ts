import { FoodHeavenActions, FoodHeavenActionType } from './FoodHeavenAction';
import { FoodHeavenPlaces } from '../model/FoodHeavenPlaces';
import { FoodHeavenState } from '../model/FoodHeavenState';
import { FoodHeavenRestaurant } from '../model/FoodHeavenRestaurant';

export const initialState: FoodHeavenState = {

    restaurantDtls: [],
    placesDtls: [],
    contactDtls: null,
    // productCart : [],
    message: '',
    loading: false,

};

export function FoodHeavenReducer(state: FoodHeavenState = initialState, foodHeavenAction: FoodHeavenActions): FoodHeavenState {
    switch (foodHeavenAction.type) {
        case FoodHeavenActionType.GET_PRODUCT_DETAILS: {
            return {
                ...state,
                loading: true
            }
        }

        case FoodHeavenActionType.GET_PRODUCT_DETAILS_SUCCESS: {
            return {
                ...state,
                restaurantDtls: foodHeavenAction.payload
            }
        }

        case FoodHeavenActionType.GET_PRODUCT_DETAILS_FAILED: {
            return { ...state };
        }


        case FoodHeavenActionType.GET_PLACES_DETAILS: {
            return {
                ...state,
                loading: true
            }
        }

        case FoodHeavenActionType.GET_PLACES_DETAILS_SUCCESS: {
            return {
                ...state,
                placesDtls: foodHeavenAction.payload
            }
        }

        case FoodHeavenActionType.GET_PLACES_DETAILS_FAILED: {
            return { ...state };
        }

        case FoodHeavenActionType.ADD_ITEM_TO_CART: {
            return {
                ...state,
                loading: true
            }
        }

        case FoodHeavenActionType.ADD_ITEM_TO_CART_SUCCESS: {
            return {
                ...state,
                // productCart: [...state.productCart, foodHeavenAction.payload]
            }
        }

        case FoodHeavenActionType.ADD_ITEM_TO_CART_FAILED: {
            return { ...state };
        }

        case FoodHeavenActionType.REMOVE_ITEM_TO_CART: {
            return {
                ...state,
                loading: true
            }
        }

        case FoodHeavenActionType.REMOVE_ITEM_TO_CART_SUCCESS: {
            return {
                ...state,
                //  productCart: [...state.productCart.filter(product => product !== foodHeavenAction.payload.filter(productCart => productCart.productName === product))]
            }
        }

        case FoodHeavenActionType.REMOVE_ITEM_TO_CART_FAILED: {
            return { ...state };
        }

        case FoodHeavenActionType.ADD_CONTACT_TO_US_DTLS: {
            console.log('ADD_CONTACT_TO_US_DTLS called ...')
            return { ...state };
        }


        case FoodHeavenActionType.ADD_CONTACT_TO_US_DTLS_SUCCESS: {
            console.log('ADD_CONTACT_TO_US_DTLS_SUCCESS called ...')
            return {
                ...state,
                contactDtls: foodHeavenAction.payload
            };
        }

        case FoodHeavenActionType.ADD_CONTACT_TO_US_DTLS_FAILED: {
            return { ...state };
        }

    }

}
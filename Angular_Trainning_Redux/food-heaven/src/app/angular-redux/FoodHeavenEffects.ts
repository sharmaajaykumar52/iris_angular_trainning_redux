import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, catchError, map } from 'rxjs/operators'
import { of } from 'rxjs';
import { FoodHeavenActionType,AddContactDtlsFailed,AddContactDtlsSuccess, GetProductDetails, GetProductDetailsSuccess, GetProductDetailsFailed, AddItemDetails, AddItemSuccess, AddItemFailed, RemoveItemDetails, RemoveItemSuccess, RemoveItemFailed, GetPlacesDetails, GetPlacesDetailsSuccess, GetPlacesDetailsFailed } from './FoodHeavenAction';
import { FoodHeavenPlaceServService } from '../food-heaven-services/food-heaven-place-serv.service';
import { FoodHeavenPlaces } from '../model/FoodHeavenPlaces';
import { FoodHeavenRestaurant } from '../model/FoodHeavenRestaurant';
import { FoodHeavenRestaurantDtlsServService } from '../food-heaven-services/food-heaven-restaurant-dtls-serv.service';
import { FoodHeavenContactUsServService } from '../food-heaven-services/food-heaven-contact-us-serv.service';
import { FoodHeavenContactUs } from '../model/FoodHeavenContactUs';

@Injectable()
export class FoodHeavenEffects {

    constructor(
        private actions$: Actions,
        private foodHeavenPlaceService: FoodHeavenPlaceServService,
        private foodHeavenRestaurantService: FoodHeavenRestaurantDtlsServService,
        private contactUsService:FoodHeavenContactUsServService
    ) { }


    foodHeavenPlaces = createEffect(() =>
        this.actions$.pipe(
            ofType(FoodHeavenActionType.GET_PLACES_DETAILS),
            switchMap(() => this.foodHeavenPlaceService.getPlacesListfromapi().pipe(
                map((places: Array<FoodHeavenPlaces>) => new GetPlacesDetailsSuccess(places)),
                catchError(error => of(new GetPlacesDetailsFailed(error)))
            ))

        )
    );

    foodHeavenRestaurant = createEffect(() =>
        this.actions$.pipe(
            ofType(FoodHeavenActionType.GET_PRODUCT_DETAILS),
            switchMap(() => this.foodHeavenRestaurantService.getRestaurantDataFromApi().pipe(
                map((restaurantDtls: Array<FoodHeavenRestaurant>) => new GetProductDetailsSuccess(restaurantDtls)),
                catchError(error => of(new GetProductDetailsFailed(error)))
            ))

        )
    );


    foodHeavenContact = createEffect(() =>
    this.actions$.pipe(
        ofType(FoodHeavenActionType.ADD_CONTACT_TO_US_DTLS_SUCCESS),
        switchMap((data) => this.contactUsService.saveContactUsDetails(data).pipe(
            map((contactDtls: FoodHeavenContactUs) => new AddContactDtlsSuccess(contactDtls)),
            catchError(error => of(new AddContactDtlsFailed(error)))
        ))

    )
);


}
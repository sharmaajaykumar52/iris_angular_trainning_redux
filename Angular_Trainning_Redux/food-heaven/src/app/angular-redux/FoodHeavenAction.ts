import { Action } from '@ngrx/store';
import { FoodHeavenPlaces } from '../model/FoodHeavenPlaces';
import { FoodHeavenRestaurant } from '../model/FoodHeavenRestaurant';
import { FoodHeavenContactUs } from '../model/FoodHeavenContactUs';

export enum FoodHeavenActionType {

    GET_PRODUCT_DETAILS = 'GET_PRODUCT_DETAILS',
    GET_PRODUCT_DETAILS_SUCCESS = 'GET_PRODUCT_DETAILS_SUCCESS',
    GET_PRODUCT_DETAILS_FAILED = 'GET_PRODUCT_DETAILS_FAILED',

    GET_PLACES_DETAILS = 'GET_PLACES_DETAILS',
    GET_PLACES_DETAILS_SUCCESS = 'GET_PLACES_DETAILS_SUCCESS',
    GET_PLACES_DETAILS_FAILED = 'GET_PLACES_DETAILS_FAILED',

    ADD_ITEM_TO_CART = 'ADD_ITEM_TO_CART',
    ADD_ITEM_TO_CART_SUCCESS = 'ADD_ITEM_TO_CART_SUCCESS',
    ADD_ITEM_TO_CART_FAILED = 'ADD_ITEM_TO_CART_FAILED',

    REMOVE_ITEM_TO_CART = 'REMOVE_ITEM_TO_CART',
    REMOVE_ITEM_TO_CART_SUCCESS = 'REMOVE_ITEM_TO_CART_SUCCESS',
    REMOVE_ITEM_TO_CART_FAILED = 'REMOVE_ITEM_TO_CART_FAILED',

    ADD_CONTACT_TO_US_DTLS = 'ADD_CONTACT_TO_US_DTLS',
    ADD_CONTACT_TO_US_DTLS_SUCCESS = 'ADD_CONTACT_TO_US_DTLS_SUCCESS',
    ADD_CONTACT_TO_US_DTLS_FAILED = 'ADD_CONTACT_TO_US_DTLS_FAILED'

}

export class GetProductDetails implements Action {
    readonly type = FoodHeavenActionType.GET_PRODUCT_DETAILS;
}

export class GetProductDetailsSuccess implements Action {
    readonly type = FoodHeavenActionType.GET_PRODUCT_DETAILS_SUCCESS;
    constructor(public payload: Array<FoodHeavenRestaurant>) {
    }
}

export class GetProductDetailsFailed implements Action {
    readonly type = FoodHeavenActionType.GET_PRODUCT_DETAILS_FAILED;
    constructor(public payload: Array<FoodHeavenRestaurant>) {
    }
}


export class GetPlacesDetails implements Action {
    readonly type = FoodHeavenActionType.GET_PLACES_DETAILS;
}

export class GetPlacesDetailsSuccess implements Action {
    readonly type = FoodHeavenActionType.GET_PLACES_DETAILS_SUCCESS;
    constructor(public payload: Array<FoodHeavenPlaces>) {

    }
}

export class GetPlacesDetailsFailed implements Action {
    readonly type = FoodHeavenActionType.GET_PLACES_DETAILS_FAILED;
    constructor(public payload: Array<FoodHeavenPlaces>) {
    }

}

export class AddItemDetails implements Action {
    readonly type = FoodHeavenActionType.ADD_ITEM_TO_CART;
}

export class AddItemSuccess implements Action {
    readonly type = FoodHeavenActionType.ADD_ITEM_TO_CART_SUCCESS;
    constructor(public payload: Array<any>) {
    }
}

export class AddItemFailed implements Action {
    readonly type = FoodHeavenActionType.ADD_ITEM_TO_CART_FAILED;
    constructor(public payload: Array<any>) {
    }
}

export class RemoveItemDetails implements Action {
    readonly type = FoodHeavenActionType.REMOVE_ITEM_TO_CART;
}

export class RemoveItemSuccess implements Action {
    readonly type = FoodHeavenActionType.REMOVE_ITEM_TO_CART_SUCCESS;
    constructor(public payload: Array<any>) {
    }
}

export class RemoveItemFailed implements Action {
    readonly type = FoodHeavenActionType.REMOVE_ITEM_TO_CART_FAILED;
    constructor(public payload: Array<any>) {
    }
}


export class AddContactDtls implements Action {
    readonly type = FoodHeavenActionType.ADD_CONTACT_TO_US_DTLS;

}

export class AddContactDtlsSuccess implements Action {
    readonly type = FoodHeavenActionType.ADD_CONTACT_TO_US_DTLS_SUCCESS;
    constructor(public payload: FoodHeavenContactUs) {
    }

}

export class AddContactDtlsFailed implements Action {
    readonly type = FoodHeavenActionType.ADD_CONTACT_TO_US_DTLS_FAILED;
    constructor(public payload: FoodHeavenContactUs) {
    }
}

export type FoodHeavenActions = GetProductDetails | GetProductDetailsSuccess
    | GetProductDetailsFailed | AddItemDetails | AddItemSuccess | AddItemFailed | RemoveItemDetails |
    RemoveItemSuccess | RemoveItemFailed | GetPlacesDetails | GetPlacesDetailsSuccess | GetPlacesDetailsFailed | AddContactDtlsSuccess
    | AddContactDtls | AddContactDtlsFailed
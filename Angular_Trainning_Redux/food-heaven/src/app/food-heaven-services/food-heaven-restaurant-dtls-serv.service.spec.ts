import { TestBed } from '@angular/core/testing';

import { FoodHeavenRestaurantDtlsServService } from './food-heaven-restaurant-dtls-serv.service';

describe('FoodHeavenRestaurantDtlsServService', () => {
  let service: FoodHeavenRestaurantDtlsServService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FoodHeavenRestaurantDtlsServService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

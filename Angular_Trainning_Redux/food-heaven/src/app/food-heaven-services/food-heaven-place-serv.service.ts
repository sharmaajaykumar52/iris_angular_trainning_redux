import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { FoodHeavenPlaces } from '../model/FoodHeavenPlaces';

@Injectable({
  providedIn: 'root'
})
export class FoodHeavenPlaceServService {

  placesDetailsUrl: string;

  constructor(private http: HttpClient) {
    this.placesDetailsUrl = 'http://localhost:3000/famousPlaces';
  }

   getPlacesListfromapi() {
    console.log('gettign places list from API')
    return  this.http.get<FoodHeavenPlaces[]>(`${this.placesDetailsUrl}`)
      .pipe(catchError((error: any) => throwError(error.message)));
  }

}

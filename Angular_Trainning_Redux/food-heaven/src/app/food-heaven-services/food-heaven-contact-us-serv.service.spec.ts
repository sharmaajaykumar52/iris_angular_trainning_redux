import { TestBed } from '@angular/core/testing';

import { FoodHeavenContactUsServService } from './food-heaven-contact-us-serv.service';

describe('FoodHeavenContactUsServService', () => {
  let service: FoodHeavenContactUsServService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FoodHeavenContactUsServService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

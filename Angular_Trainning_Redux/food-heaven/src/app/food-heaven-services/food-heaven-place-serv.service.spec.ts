import { TestBed } from '@angular/core/testing';

import { FoodHeavenPlaceServService } from './food-heaven-place-serv.service';

describe('FoodHeavenPlaceServService', () => {
  let service: FoodHeavenPlaceServService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FoodHeavenPlaceServService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

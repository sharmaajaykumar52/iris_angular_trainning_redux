import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { FoodHeavenRestaurant } from '../model/FoodHeavenRestaurant';


@Injectable({
  providedIn: 'root'
})
export class FoodHeavenRestaurantDtlsServService {

  restaurantDetailsUrl: string;

  constructor(private http: HttpClient) {
    this.restaurantDetailsUrl = 'http://localhost:3000/restaurantData';
  }

  getRestaurantDataFromApi() {
    console.log('gettign places list from API')
    return this.http.get<FoodHeavenRestaurant[]>(`${this.restaurantDetailsUrl}`)
      .pipe(catchError((error: any) => throwError(error.message)));
  }

}

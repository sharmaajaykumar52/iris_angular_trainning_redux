import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { FoodHeavenContactUs } from '../model/FoodHeavenContactUs';

@Injectable({
  providedIn: 'root'
})
export class FoodHeavenContactUsServService {

  contactUsDetailsUrl: string

  constructor(private http: HttpClient) {
    this.contactUsDetailsUrl = 'http://localhost:3000/contactUs';
   }

  saveContactUsDetails(contactUs: FoodHeavenContactUs) {
    console.log('contactUs --------> '+JSON.stringify(contactUs['payload']));
    return this.http.post<FoodHeavenContactUs>(`${this.contactUsDetailsUrl}`, contactUs['payload']).pipe(catchError((error: any) => throwError(error.message)));

  }


}

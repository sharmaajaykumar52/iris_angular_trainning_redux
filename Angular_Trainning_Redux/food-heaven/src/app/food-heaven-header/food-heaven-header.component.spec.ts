import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodHeavenHeaderComponent } from './food-heaven-header.component';

describe('FoodHeavenHeaderComponent', () => {
  let component: FoodHeavenHeaderComponent;
  let fixture: ComponentFixture<FoodHeavenHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodHeavenHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodHeavenHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

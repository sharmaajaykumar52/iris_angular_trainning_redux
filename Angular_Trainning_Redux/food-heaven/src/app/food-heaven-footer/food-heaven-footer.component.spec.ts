import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodHeavenFooterComponent } from './food-heaven-footer.component';

describe('FoodHeavenFooterComponent', () => {
  let component: FoodHeavenFooterComponent;
  let fixture: ComponentFixture<FoodHeavenFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodHeavenFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodHeavenFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodHeavenContactUsComponent } from './food-heaven-contact-us.component';

describe('FoodHeavenContactUsComponent', () => {
  let component: FoodHeavenContactUsComponent;
  let fixture: ComponentFixture<FoodHeavenContactUsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodHeavenContactUsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodHeavenContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

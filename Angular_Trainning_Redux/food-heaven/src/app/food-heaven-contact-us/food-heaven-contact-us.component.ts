import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as FoodHeavenActions from '../angular-redux/FoodHeavenAction';
import { FormGroup, FormControl, Validators } from '@angular/forms'

@Component({
  selector: 'app-food-heaven-contact-us',
  templateUrl: './food-heaven-contact-us.component.html',
  styleUrls: ['./food-heaven-contact-us.component.css']
})
export class FoodHeavenContactUsComponent implements OnInit {

  contactUsFormFields = {
    emailId: new FormControl('', [Validators.required]),
    phoneNumber: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    address: new FormControl('', [Validators.required]),
    query: new FormControl('', [Validators.required])
  };

  contactusForm = new FormGroup(this.contactUsFormFields);

  constructor(private store: Store<any>) { }

  ngOnInit(): void {
  }

  onSubmitContactUsForm() {
    console.log(this.contactusForm.value);
    
    console.log('contact us form reste successfully !!!');
    this.store.dispatch(new FoodHeavenActions.AddContactDtlsSuccess(this.contactusForm.value));
    this.store.select('foodHeavenState').subscribe(response => {
    console.log('response is '+response);      
    });
    this.contactusForm.reset();
  }

}

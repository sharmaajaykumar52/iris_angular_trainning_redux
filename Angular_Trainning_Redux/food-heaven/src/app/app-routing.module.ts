import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FoodHeavenAboutUsComponent } from './food-heaven-about-us/food-heaven-about-us.component';
import { FoodHeavenContactUsComponent } from './food-heaven-contact-us/food-heaven-contact-us.component';
import { FoodHeavenOrderFoodComponent } from './food-heaven-order-food/food-heaven-order-food.component';
import { FoodHeavenHomeComponent } from './food-heaven-home/food-heaven-home.component';


const routes: Routes = [
    { path: '', component: FoodHeavenHomeComponent },
    { path: 'aboutus', component: FoodHeavenAboutUsComponent },
    { path: 'contactus', component: FoodHeavenContactUsComponent },
    { path: 'orderFood', component: FoodHeavenOrderFoodComponent },
   // { path: '',  redirectTo: 'home', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

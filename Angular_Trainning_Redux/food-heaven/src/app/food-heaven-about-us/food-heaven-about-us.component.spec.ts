import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodHeavenAboutUsComponent } from './food-heaven-about-us.component';

describe('FoodHeavenAboutUsComponent', () => {
  let component: FoodHeavenAboutUsComponent;
  let fixture: ComponentFixture<FoodHeavenAboutUsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodHeavenAboutUsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodHeavenAboutUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FoodHeavenOrderFoodComponent } from './food-heaven-order-food.component';

describe('FoodHeavenOrderFoodComponent', () => {
  let component: FoodHeavenOrderFoodComponent;
  let fixture: ComponentFixture<FoodHeavenOrderFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodHeavenOrderFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodHeavenOrderFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

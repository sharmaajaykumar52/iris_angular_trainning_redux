import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as FoodHeavenActions from '../angular-redux/FoodHeavenAction';
import { FoodHeavenPlaces } from '../model/FoodHeavenPlaces';
import { FoodHeavenRestaurant } from '../model/FoodHeavenRestaurant';

@Component({
  selector: 'app-food-heaven-order-food',
  templateUrl: './food-heaven-order-food.component.html',
  styleUrls: ['./food-heaven-order-food.component.css']
})
export class FoodHeavenOrderFoodComponent implements OnInit {

  placesList: Array<FoodHeavenPlaces>;
  restaurantDtlsList: Array<FoodHeavenRestaurant>;
  loading: boolean;
  error: string;
  uniqueStates: string[];
  uniqueCities: string[];
  isRestaurantDtlsAvailable: boolean;  
  isSelectedStateAvailable: boolean;
  isSelectedCityAvailable: boolean;
  selectedRestaurantDtls: Array<FoodHeavenRestaurant>;

  constructor(private store: Store<any>) { }

  ngOnInit(): void {
    this.isRestaurantDtlsAvailable = false;
    this.isSelectedCityAvailable=false;
    this.isSelectedStateAvailable=false;
    console.log('Getting places details from API on component loaded an deventevent dispatched .....');
    this.store.dispatch(new FoodHeavenActions.GetPlacesDetails);
    this.store.select('foodHeavenState').subscribe(response => {
      this.placesList = response['placesDtls'];
      this.uniqueStates = [... new Set(this.placesList.map(item => item.state))].sort();
    });

    console.log('Fetching the dispatch restaurant details .....');
    this.store.dispatch(new FoodHeavenActions.GetProductDetails);
    this.store.select('foodHeavenState').subscribe(response => {
      this.restaurantDtlsList = response['restaurantDtls'];
    });
  }

  onStateSelected(selectedState) {
    this.isSelectedStateAvailable=true;
    console.log('selectedState ----> ' + selectedState);
    this.uniqueCities = [... new Set(this.placesList.filter(selectedPlace => selectedPlace.state === selectedState).map(item => item.name))].sort();
  }

  onCitySelected(selectedCity) {
    console.log('selectedCity ----->' + selectedCity);
    this.selectedRestaurantDtls = this.restaurantDtlsList.filter(restDtls => restDtls.location === selectedCity);
    console.log('this.selectedRestaurantDtls ' + this.selectedRestaurantDtls.length);
    this.isRestaurantDtlsAvailable=true; 
    this.isSelectedCityAvailable=true;
  }



}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FoodHeavenAboutUsComponent } from './food-heaven-about-us/food-heaven-about-us.component';
import { FoodHeavenContactUsComponent } from './food-heaven-contact-us/food-heaven-contact-us.component';
import { FoodHeavenHeaderComponent } from './food-heaven-header/food-heaven-header.component';
import { FoodHeavenFooterComponent } from './food-heaven-footer/food-heaven-footer.component';
import { FoodHeavenNavBarComponent } from './common/food-heaven-nav-bar/food-heaven-nav-bar.component';
import { FoodHeavenOrderFoodComponent } from './food-heaven-order-food/food-heaven-order-food.component';
import { FoodHeavenHomeComponent } from './food-heaven-home/food-heaven-home.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects'
import { Store } from '@ngrx/store';
import { StoreModule } from '@ngrx/store';
import { reducers } from './angular-redux/FoodHeavenActionReducerMapper';
import { FoodHeavenEffects } from './angular-redux/FoodHeavenEffects';
import { HttpClientModule } from "@angular/common/http";
import { FoodHeavenCardComponent } from './common/food-heaven-card/food-heaven-card.component';


@NgModule({
  declarations: [
    AppComponent,
    FoodHeavenHeaderComponent,
    FoodHeavenFooterComponent,
    FoodHeavenAboutUsComponent,
    FoodHeavenContactUsComponent,    
    FoodHeavenNavBarComponent,
    FoodHeavenOrderFoodComponent,
    FoodHeavenHomeComponent,
    FoodHeavenCardComponent    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([FoodHeavenEffects]),
    HttpClientModule
  ],
  providers: [Store],
  bootstrap: [AppComponent]
})
export class AppModule { }

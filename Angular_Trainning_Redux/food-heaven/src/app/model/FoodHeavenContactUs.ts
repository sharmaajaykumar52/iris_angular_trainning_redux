export interface FoodHeavenContactUs {
    emailId: string;
    phoneNumber: number;
    name: string;
    address: string;
    query: string
}
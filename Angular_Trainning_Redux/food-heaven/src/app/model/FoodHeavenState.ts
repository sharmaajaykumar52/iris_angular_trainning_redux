import { FoodHeavenPlaces } from '../model/FoodHeavenPlaces';
import {FoodHeavenRestaurant } from '../model/FoodHeavenRestaurant';
import { FoodHeavenContactUs } from '../model/FoodHeavenContactUs';

export interface FoodHeavenState {
    loading: boolean,
    restaurantDtls: Array<FoodHeavenRestaurant>,
    placesDtls: Array<FoodHeavenPlaces>,
    contactDtls: FoodHeavenContactUs,
    message: ''
  
}
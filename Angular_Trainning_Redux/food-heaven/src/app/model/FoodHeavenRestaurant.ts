export interface FoodHeavenRestaurant {
    reviews: number;
    parkinglot: boolean;
    location: string;
    phone: string;
    averagecost: number;
    image: string;
    imageId: string;
    restauranttype: string;
    _id: string;
    businessname: string;
    address: string;
    menu: string;
    slug: string;
    email: string;
    _v: number;
    foodMenu: any[];
    id: string;
}
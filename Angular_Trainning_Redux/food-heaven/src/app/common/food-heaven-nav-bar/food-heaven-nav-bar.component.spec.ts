import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodHeavenNavBarComponent } from './food-heaven-nav-bar.component';

describe('FoodHeavenNavBarComponent', () => {
  let component: FoodHeavenNavBarComponent;
  let fixture: ComponentFixture<FoodHeavenNavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodHeavenNavBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodHeavenNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

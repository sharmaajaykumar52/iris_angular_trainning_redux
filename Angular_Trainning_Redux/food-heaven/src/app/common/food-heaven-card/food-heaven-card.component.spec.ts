import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodHeavenCardComponent } from './food-heaven-card.component';

describe('FoodHeavenCardComponent', () => {
  let component: FoodHeavenCardComponent;
  let fixture: ComponentFixture<FoodHeavenCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodHeavenCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodHeavenCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

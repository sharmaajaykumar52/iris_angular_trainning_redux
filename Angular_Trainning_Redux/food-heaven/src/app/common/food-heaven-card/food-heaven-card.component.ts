import { Component, OnInit,Input } from '@angular/core';
import { FoodHeavenRestaurant } from '../../model/FoodHeavenRestaurant';



@Component({
  selector: 'app-food-heaven-card',
  templateUrl: './food-heaven-card.component.html',
  styleUrls: ['./food-heaven-card.component.css']
})
export class FoodHeavenCardComponent implements OnInit {

  constructor() { }

  @Input() public selectedRestaurantDtls : Array<FoodHeavenRestaurant>;

  ngOnInit(): void {
    console.log("@#@#@#@#  "+this.selectedRestaurantDtls);
  }

}
